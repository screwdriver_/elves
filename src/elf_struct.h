#ifndef ELF_STRUCT_H
# define ELF_STRUCT_H

# include <stdint.h>

# define EI_NIDENT		16
# define EI_MAG0		0
# define EI_MAG1		1
# define EI_MAG2		2
# define EI_MAG3		3
# define EI_CLASS		4
# define EI_DATA		5
# define EI_VERSION		6
# define EI_OSABI		7
# define EI_ABIVERSION	8
# define EI_ABIPAD		9

# define ET_NONE	0x0000
# define ET_REL		0x0001
# define ET_EXEC	0x0002
# define ET_DYN		0x0003
# define ET_CORE	0x0004
# define ET_LOOS	0xfe00
# define ET_HIOS	0xfeff
# define ET_LOPROC	0xff00
# define ET_HIPROC	0xffff

# define PT_NULL	0x00000000
# define PT_LOAD	0x00000001
# define PT_DYNAMIC	0x00000002
# define PT_INTERP	0x00000003
# define PT_NOTE	0x00000004
# define PT_SHLIB	0x00000005
# define PT_PHDR	0x00000006
# define PT_TLS		0x00000007
# define PT_LOOS	0x60000000
# define PT_HIOS	0x6fffffff
# define PT_LOPROC	0x70000000
# define PT_HIPROC	0x7fffffff

# define SHT_NULL			0x00000000
# define SHT_PROGBITS		0x00000001
# define SHT_SYMTAB			0x00000002
# define SHT_STRTAB			0x00000003
# define SHT_RELA			0x00000004
# define SHT_HASH			0x00000005
# define SHT_DYNAMIC		0x00000006
# define SHT_NOTE			0x00000007
# define SHT_NOBITS			0x00000008
# define SHT_REL			0x00000009
# define SHT_SHLIB			0x0000000a
# define SHT_DYNSYM			0x0000000b
# define SHT_INIT_ARRAY		0x0000000e
# define SHT_FINI_ARRAY		0x0000000f
# define SHT_PREINIT_ARRAY	0x00000010
# define SHT_GROUP			0x00000011
# define SHT_SYMTAB_SHNDX	0x00000012
# define SHT_NUM			0x00000013
# define SHT_LOOS			0x60000000

# define SHF_WRITE				0x00000001
# define SHF_ALLOC				0x00000002
# define SHF_EXECINSTR			0x00000004
# define SHF_MERGE				0x00000010
# define SHF_STRINGS			0x00000020
# define SHF_INFO_LINK			0x00000040
# define SHF_LINK_ORDER			0x00000080
# define SHF_OS_NONCONFORMING	0x00000100
# define SHF_GROUP				0x00000200
# define SHF_TLS				0x00000400
# define SHF_MASKOS				0x0ff00000
# define SHF_MASKPROC			0xf0000000
# define SHF_ORDERED			0x04000000
# define SHF_EXCLUDE			0x08000000

# define STB_LOCAL	0
# define STB_GLOBAL	1
# define STB_WEAK	2
# define STB_LOOS	10
# define STB_HIOS	12
# define STB_LOPROC	13
# define STB_HIPROC	15

# define STT_NOTYPE		0
# define STT_OBJECT		1
# define STT_FUNC		2
# define STT_SECTION	3
# define STT_FILE		4
# define STT_LOOS		10
# define STT_HIOS		12
# define STT_LOPROC		13
# define STT_HIPROC		15

/*
 * 32 bits ELF header.
 */
__attribute__((packed))
struct elf32_eh
{
	unsigned char e_ident[EI_NIDENT];
	uint16_t e_type;
	uint16_t e_machine;
	uint32_t e_version;
	uint32_t e_entry;
	uint32_t e_phoff;
	uint32_t e_shoff;
	uint32_t e_flags;
	uint16_t e_ehsize;
	uint16_t e_phentsize;
	uint16_t e_phnum;
	uint16_t e_shentsize;
	uint16_t e_shnum;
	uint16_t e_shstrndx;
};

/*
 * 64 bits ELF header.
 */
__attribute__((packed))
struct elf64_eh
{
	unsigned char e_ident[EI_NIDENT];
	uint16_t e_type;
	uint16_t e_machine;
	uint32_t e_version;
	uint64_t e_entry;
	uint64_t e_phoff;
	uint64_t e_shoff;
	uint32_t e_flags;
	uint16_t e_ehsize;
	uint16_t e_phentsize;
	uint16_t e_phnum;
	uint16_t e_shentsize;
	uint16_t e_shnum;
	uint16_t e_shstrndx;
};

typedef struct elf32_eh elf32_eh_t;
typedef struct elf64_eh elf64_eh_t;

/*
 * 32 bits ELF program header.
 */
__attribute__((packed))
struct elf32_ph
{
	uint32_t p_type;
	uint32_t p_offset;
	uint32_t p_vaddr;
	uint32_t p_paddr;
	uint32_t p_filesz;
	uint32_t p_memsz;
	uint32_t p_flags;
	uint32_t p_align;
};

/*
 * 64 bits ELF program header.
 */
__attribute__((packed))
struct elf64_ph
{
	uint32_t p_type;
	uint32_t p_flags;
	uint64_t p_offset;
	uint64_t p_vaddr;
	uint64_t p_paddr;
	uint64_t p_filesz;
	uint64_t p_memsz;
	uint64_t p_align;
};

typedef struct elf32_ph elf32_ph_t;
typedef struct elf64_ph elf64_ph_t;

/*
 * 32 bits ELF section header.
 */
__attribute__((packed))
struct elf32_sh
{
	uint32_t sh_name;
	uint32_t sh_type;
	uint32_t sh_flags;
	uint32_t sh_addr;
	uint32_t sh_offset;
	uint32_t sh_size;
	uint32_t sh_link;
	uint32_t sh_info;
	uint32_t sh_addralign;
	uint32_t sh_entsize;
};

/*
 * 64 bits ELF section header.
 */
__attribute__((packed))
struct elf64_sh
{
	uint32_t sh_name;
	uint32_t sh_type;
	uint64_t sh_flags;
	uint64_t sh_addr;
	uint64_t sh_offset;
	uint64_t sh_size;
	uint32_t sh_link;
	uint32_t sh_info;
	uint64_t sh_addralign;
	uint64_t sh_entsize;
};

typedef struct elf32_sh elf32_sh_t;
typedef struct elf64_sh elf64_sh_t;

/*
 * 32 bits ELF symbol table entry.
 */
__attribute__((packed))
struct elf32_sym
{
	uint32_t st_name;
	uint32_t st_value;
	uint32_t st_size;
	unsigned char st_info;
	unsigned char st_other;
	uint16_t st_shndx;
};

/*
 * 64 bits ELF symbol table entry.
 */
__attribute__((packed))
struct elf64_sym
{
	uint32_t st_name;
	unsigned char st_info;
	unsigned char st_other;
	uint16_t st_shndx;
	uint64_t st_value;
	uint64_t st_size;
};

typedef struct elf32_sym elf32_sym_t;
typedef struct elf64_sym elf64_sym_t;

/*
 * 32 bits ELF relacation entry.
 */
__attribute__((packed))
struct elf32_rel
{
	uint32_t r_offset;
	uint32_t r_info;
};

/*
 * 64 bits ELF relacation entry.
 */
__attribute__((packed))
struct elf64_rel
{
	uint64_t r_offset;
	uint64_t r_info;
};

typedef struct elf32_rel elf32_rel_t;
typedef struct elf64_rel elf64_rel_t;

/*
 * 32 bits ELF relacation entry.
 */
__attribute__((packed))
struct elf32_rela
{
	uint32_t r_offset;
	uint32_t r_info;
	int32_t r_addend;
};

/*
 * 64 bits ELF relacation entry.
 */
__attribute__((packed))
struct elf64_rela
{
	uint64_t r_offset;
	uint64_t r_info;
	int64_t r_addend;
};

typedef struct elf32_rela elf32_rela_t;
typedef struct elf64_rela elf64_rela_t;

#endif
