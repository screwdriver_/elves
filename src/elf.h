#ifndef ELF_H
# define ELF_H

# include <elf_struct.h>

# include <stddef.h>

// TODO ELF creation

/*
 * Returns `1` if the given buffer `ptr` of size `size` contains a valid ELF
 * file content, else returns `0`.
 * If correct, the function places either 32 or 64 in the variable pointed by
 * `format`.
 */
int elf_hdr_check(const void *ptr, size_t size, int *format);

/*
 * Get section by name `name`.
 */
elf32_sh_t *elf32_get_section_by_name(const elf32_eh_t *hdr, const char *name);
elf64_sh_t *elf64_get_section_by_name(const elf64_eh_t *hdr, const char *name);

/*
 * Get section by type `type`.
 */
elf32_sh_t *elf32_get_section_by_type(const elf32_eh_t *hdr, uint32_t type);
elf64_sh_t *elf64_get_section_by_type(const elf64_eh_t *hdr, uint32_t type);

/*
 * Calls the given function `f` for every section of the ELF file. Passes
 * `handle` as parameter at every call.
 */
void elf32_foreach_section(const elf32_eh_t *hdr,
	void (*f)(void *, elf32_sh_t *), void *handle);
void elf64_foreach_section(const elf64_eh_t *hdr,
	void (*f)(void *, elf64_sh_t *), void *handle);

/*
 * Returns the name of section `section`.
 */
const char *elf32_get_section_name(const elf32_eh_t *hdr,
	const elf32_sh_t *section);
const char *elf64_get_section_name(const elf64_eh_t *hdr,
	const elf64_sh_t *section);

/*
 * Returns the pointer to a symbol by name `name`.
 */
elf32_sym_t *elf32_get_symbol(const elf32_eh_t *hdr, const char *name);
elf64_sym_t *elf64_get_symbol(const elf64_eh_t *hdr, const char *name);

/*
 * Calls the given function `f` for every symbol of the ELF file. Passes
 * `handle` as argument at every call.
 */
void elf32_foreach_symbol(const elf32_eh_t *hdr,
	void (*f)(void *, elf32_sym_t *), void *handle);
void elf64_foreach_symbol(const elf64_eh_t *hdr,
	void (*f)(void *, elf64_sym_t *), void *handle);

/*
 * Returns the name of section `section`.
 */
const char *elf32_get_symbol_name(const elf32_eh_t *hdr,
	const elf32_sym_t *symbol);
const char *elf64_get_symbol_name(const elf64_eh_t *hdr,
	const elf64_sym_t *symbol);

// TODO Relocations

#endif
