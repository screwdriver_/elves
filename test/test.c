#include <elf.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char *read_file(const char *path, size_t *len)
{
	int fd;
	char *buff;
	ssize_t l;

	if(!path || !len)
		return NULL;
	if((fd = open(path, O_RDONLY)) < 0)
		return NULL;
	*len = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	if(!(buff = malloc(*len)))
		goto fail;
	while((l = read(fd, buff, *len)) > 0)
		;
	close(fd);
	return buff;

fail:
	close(fd);
	return NULL;
}

static void print_section32(void *handle, elf32_sh_t *section)
{
	printf("- %s\n", elf32_get_section_name(handle, section));
}

static void print_section64(void *handle, elf64_sh_t *section)
{
	printf("- %s\n", elf64_get_section_name(handle, section));
}

static void print_symbol32(void *handle, elf32_sym_t *symbol)
{
	printf("- %#08lx %s\n", (long unsigned)symbol->st_value, elf32_get_symbol_name(handle, symbol));
}

static void print_symbol64(void *handle, elf64_sym_t *symbol)
{
	printf("- %#08lx %s\n", (long unsigned)symbol->st_value, elf64_get_symbol_name(handle, symbol));
}

int main(int argc, char **argv)
{
	const char *buff = NULL;
	size_t len;
	int format;

	if(argc < 2)
	{
		dprintf(STDERR_FILENO, "No input file\n");
		goto fail;
	}
	if(!(buff = read_file(argv[1], &len)))
	{
		dprintf(STDERR_FILENO, "Failed to read `%s`\n", argv[1]);
		goto fail;
	}
	if(!elf_hdr_check(buff, len, &format))
	{
		dprintf(STDERR_FILENO, "Invalid ELF file\n");
		goto fail;
	}
	printf("ELF size: %i bits\n", format);
	printf("Sections:\n");
	if(format == 64)
		elf64_foreach_section((const elf64_eh_t *) buff, print_section64,
			(void *) buff);
	else
		elf32_foreach_section((const elf32_eh_t *) buff, print_section32,
			(void *) buff);
	printf("\nSymbols:\n");
	if(format == 64)
		elf64_foreach_symbol((const elf64_eh_t *) buff, print_symbol64,
			(void *) buff);
	else
		elf32_foreach_symbol((const elf32_eh_t *) buff, print_symbol32,
			(void *) buff);
	free((void *) buff);
	return 0;

fail:
	free((void *) buff);
	return 1;
}
