#include <elf.h>
#include <string.h>

#define FUNC_CHECK(format)\
	static int check_##format(const elf##format##_eh_t *ptr, size_t size)\
	{\
		if(ptr->e_ehsize != sizeof(elf##format##_eh_t)\
			|| size < sizeof(elf##format##_eh_t))\
			return 0;\
		if(ptr->e_entry < size)\
			return 0;\
		if(ptr->e_phentsize != sizeof(elf##format##_ph_t))\
			return 0;\
		if(ptr->e_phoff + ptr->e_phentsize * ptr->e_phnum >= size)\
			return 0;\
		if(ptr->e_shentsize != sizeof(elf##format##_sh_t))\
			return 0;\
		if(ptr->e_shoff + ptr->e_shentsize * ptr->e_shnum >= size)\
			return 0;\
		if(ptr->e_shstrndx >= ptr->e_shnum)\
			return 0;\
		return 1;\
	}

FUNC_CHECK(32)
FUNC_CHECK(64)

int elf_hdr_check(const void *ptr, size_t size, int *format)
{
	if(!ptr || size < 4)
		return 0;
	if(memcmp(ptr, "\x7f""ELF", 4) != 0)
		return 0;
	if(format)
		*format = 32 * (1 << (((const unsigned char *) ptr)[EI_CLASS] - 1));
	if(*format == 32 && check_32(ptr, size))
		return 0;
	else if(*format == 64 && check_64(ptr, size))
		return 0;
	return 1;
}
