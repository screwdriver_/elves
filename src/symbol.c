#include <elf.h>
#include <string.h>

// TODO rm
#include <stdio.h>

// TODO SHT_DYNSYM

// TODO Return symbol if name corresponds
#define FUNC_GET_SYMBOL(format)\
	elf##format##_sym_t *elf##format##_get_symbol(const elf##format##_eh_t *hdr,\
		const char *name)\
	{\
		elf##format##_sh_t *symtab_section;\
		size_t symbols_count, i;\
		elf##format##_sym_t *sym;\
		const char *n;\
\
		if(!hdr || !name)\
			return NULL;\
		symtab_section = elf##format##_get_section_by_type(hdr, SHT_SYMTAB);\
		if(!symtab_section)\
			return NULL;\
		symbols_count = symtab_section->sh_size / symtab_section->sh_entsize;\
		for(i = 0; i < symbols_count; ++i)\
		{\
			sym = (void *) hdr + symtab_section->sh_offset\
				+ i * sizeof(elf##format##_sym_t);\
			if(!(n = elf##format##_get_symbol_name(hdr, sym)))\
				continue;\
			if(strcmp(n, name) == 0)\
				return sym;\
		}\
		return NULL;\
	}

FUNC_GET_SYMBOL(32)
FUNC_GET_SYMBOL(64)

#define FUNC_FOREACH_SYMBOL(format)\
	void elf##format##_foreach_symbol(const elf##format##_eh_t *hdr,\
		void (*f)(void *, elf##format##_sym_t *), void *handle)\
	{\
		elf##format##_sh_t *symtab_section;\
		size_t symbols_count, i;\
		elf##format##_sym_t *sym;\
\
		if(!hdr || !f)\
			return;\
		symtab_section = elf##format##_get_section_by_type(hdr, SHT_SYMTAB);\
		if(!symtab_section)\
			return;\
		symbols_count = symtab_section->sh_size / symtab_section->sh_entsize;\
		for(i = 0; i < symbols_count; ++i)\
		{\
			sym = (void *) hdr + symtab_section->sh_offset\
				+ i * sizeof(elf##format##_sym_t);\
			f(handle, sym);\
		}\
	}

FUNC_FOREACH_SYMBOL(32)
FUNC_FOREACH_SYMBOL(64)

#define FUNC_GET_SYMBOL_NAME(format)\
	const char *elf##format##_get_symbol_name(const elf##format##_eh_t *hdr,\
		const elf##format##_sym_t *symbol)\
	{\
		elf##format##_sh_t *strtab_section;\
		const char *section_begin, *ptr;\
\
		if(!hdr || !symbol)\
			return NULL;\
		strtab_section = elf##format##_get_section_by_type(hdr, SHT_STRTAB);\
		if(!strtab_section)\
			return NULL;\
		if(symbol->st_name != 0 && symbol->st_name >= strtab_section->sh_size)\
			return NULL;\
		section_begin = (void *) hdr + strtab_section->sh_offset;\
		ptr = section_begin + symbol->st_name;\
		if((uintptr_t) ptr + strlen(ptr)\
			> (uintptr_t) section_begin + strtab_section->sh_size)\
			return NULL;\
		return ptr;\
	}

FUNC_GET_SYMBOL_NAME(32)
FUNC_GET_SYMBOL_NAME(64)
