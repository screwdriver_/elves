#include <elf.h>
#include <string.h>

#define FUNC_GET_SECTION_BY_NAME(format)\
	elf##format##_sh_t *elf##format##_get_section_by_name(\
		const elf##format##_eh_t *hdr, const char *name)\
	{\
		size_t i;\
		void *sh;\
\
		if(!hdr || !name)\
			return NULL;\
		for(i = 0; i < hdr->e_shnum; ++i)\
		{\
			sh = (void *) hdr + hdr->e_shoff + i * hdr->e_shentsize;\
			if(strcmp(elf##format##_get_section_name(hdr, sh), name) == 0)\
				return sh;\
		}\
		return NULL;\
	}

FUNC_GET_SECTION_BY_NAME(32)
FUNC_GET_SECTION_BY_NAME(64)

#define FUNC_GET_SECTION_BY_TYPE(format)\
	elf##format##_sh_t *elf##format##_get_section_by_type(\
		const elf##format##_eh_t *hdr, uint32_t type)\
	{\
		size_t i;\
		elf##format##_sh_t *sh;\
\
		if(!hdr)\
			return NULL;\
		for(i = 0; i < hdr->e_shnum; ++i)\
		{\
			sh = (void *) hdr + hdr->e_shoff + i * hdr->e_shentsize;\
			if(sh->sh_type == type)\
				return sh;\
		}\
		return NULL;\
	}

FUNC_GET_SECTION_BY_TYPE(32)
FUNC_GET_SECTION_BY_TYPE(64)

#define FUNC_FOREACH_SECTION(format)\
	void elf##format##_foreach_section(const elf##format##_eh_t *hdr,\
		void (*f)(void *, elf##format##_sh_t *), void *handle)\
	{\
		size_t i;\
\
		if(!hdr || !f)\
			return;\
		for(i = 0; i < hdr->e_shnum; ++i)\
			f(handle, (void *) hdr + hdr->e_shoff + i * hdr->e_shentsize);\
	}

FUNC_FOREACH_SECTION(32)
FUNC_FOREACH_SECTION(64)

#define FUNC_GET_SECTION_NAME(format)\
	const char *elf##format##_get_section_name(const elf##format##_eh_t *hdr,\
		const elf##format##_sh_t *section)\
	{\
		elf##format##_sh_t *name_section;\
		const char *name_section_begin, *ptr;\
\
		if(!hdr || !section)\
			return NULL;\
		name_section = (void *) hdr + hdr->e_shoff\
			+ hdr->e_shentsize * hdr->e_shstrndx;\
		if(section->sh_name >= name_section->sh_size)\
			return NULL;\
		name_section_begin = (void *) hdr + name_section->sh_offset;\
		ptr = name_section_begin + section->sh_name;\
		if((uintptr_t) ptr + strlen(ptr)\
			> (uintptr_t) name_section_begin + name_section->sh_size)\
			return NULL;\
		return ptr;\
	}

FUNC_GET_SECTION_NAME(32)
FUNC_GET_SECTION_NAME(64)
